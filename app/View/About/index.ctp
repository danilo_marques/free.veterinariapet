<div class="container">
	<div class="row services-single">
		<div class="col-md-6">
			<img alt="Dog walking" src="/img/template/dog-walking.png">
		</div>
		<div class="col-md-6">
			<h2>O que é o VeterináriaPet?</h2>
			<p>O VeterináriaPet é um produto voltado para a saúde e bem estar de seu bichinho de estimação, onde você encontra as melhores clínicas veterinárias para o seu Pet.</p>

			<p>Pelo site você consegue localizar uma clínica veterinária mais próxima de você, comparar preços e agendar uma consulta para o meu bichinho de maneira fácil e rápida, sem precisar sair de casa.</p>

			<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas.</p>
			<div class="hours">
				<div>
					<p class="day"><strong>Funcionamento</strong></p>
					<p class="time">24x7x365</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span>Para profissionais</span></h3>
			<p>O VeterináriaPet é um sistema de prontuário eletrônico para animais e agenda online que visa facilitar a trabalho de profissionais do ramo. Juntamente com o sistema você recebe um perfil online no site da sua clinica o qual as informações são personalizaveis e também é possivel definir se aparece a opção de agendamento de consulta ou não e se o perfil aparecerá online ou não. Para maiores informações clique no botão abaixo.</p>

			<form action="/profissionais" method="get">
				<button class="btn btn-default btn-green" type="submit">Saiba Mais</button>
			</form>
		</div>
	</div>
</div>

<div class="br-2"></div>

<!-- Mostra o link ativo no menu -->
<div id="active-link" active="li-about"></div>