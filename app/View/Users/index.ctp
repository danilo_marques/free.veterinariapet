<div class="container" style="width: auto !important;">
    <div class="row centered margin-top-zero">
        <div class="col-md-6 pre-login-personal">
            <h1><strong>Pessoal</strong></h1>
            <div class="col-md-6 col-md-offset-3">
                <p>Sistema de login para donos de Pets. Acesso a consultas.</p>

                <form action="/login/pessoal" method="get">
                    <button class="btn btn-white" type="submit"><strong>Fazer Login</strong></button>
                </form>
            </div>
        </div>
        <div class="col-md-6 pre-login-professional">
            <h1><strong>Profissional</strong></h1>
            <div class="col-md-6 col-md-offset-3">
                <p>Sistema de login para acesso ao sistema de prontuário e agenda.</p>

                <form action="/login/profissional" method="get">
                    <button class="btn btn-white" type="submit"><strong>Fazer Login</strong></button>
                </form>
            </div>
        </div>
    </div>
</div>