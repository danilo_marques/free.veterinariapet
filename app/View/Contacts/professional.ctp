<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span><strong>Veterinária</strong>Pet para profissionais</span></h3>
			<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
		</div>
	</div>
</div>
<div data-stellar-background-ratio=".3" class="prices" style="background-position: 0px 0px;">
	<div class="container">
		<div class="row">
			<div class="col-md-3 costs">
				<div data-stellar-horizontal-offset="0" data-stellar-vertical-offset="150" data-stellar-ratio="1.2" style="top: 0px;">
					<img alt="Small dog pricing" src="/img/template/small-dog.png">
					<h4>Plano Basic</h4>
					<p class="per-night">R$ 35 ao mês</p>
					<p>Agendamento Online</p>
					<p>Prontontuário Eletônico</p>
					<p>Agenda de consultas</p>
					<p>2 acessos simultâneos</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<form action="index.html" method="get">
						<button class="btn btn-default btn-green" type="submit">Contratar</button>
					</form>
				</div>
			</div>
			<div class="col-md-3 costs">
				<div data-stellar-horizontal-offset="0" data-stellar-vertical-offset="150" data-stellar-ratio="1.2" style="top: 0px;">
					<img alt="Medium dog pricing" src="/img/template/medium-dog.png">
					<h4>Plano Basic + Emails</h4>
					<p class="per-night">R$ 40 ao mês</p>
					<p>Agendamento Online</p>
					<p>Prontontuário Eletônico</p>
					<p>Agenda de consultas</p>
					<p>2 acessos simultâneos</p>
					<p>Envio de emails de consulta</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<p>&nbsp;</p>
					<form action="index.html" method="get">
						<button class="btn btn-default btn-green" type="submit">Contratar</button>
					</form>
				</div>
			</div>
			<div class="col-md-3 costs">
				<div data-stellar-horizontal-offset="0" data-stellar-vertical-offset="150" data-stellar-ratio="1.2" style="top: 0px;">
					<img alt="Large dog pricing" src="/img/template/large-dog.png">
					<h4>Plano Conforto</h4>
					<p class="per-night">R$ 60 ao mês</p>
					<p>Agendamento Online</p>
					<p>Prontontuário Eletônico</p>
					<p>Agenda de consultas</p>
					<p>4 acessos simultâneos</p>
					<p>Envio de emails de consulta</p>
					<p>100 SMS's para envio</p>
					<p>Envio de SMS de confirmação</p>
					<p>Resposta de SMS gratuita</p>
					<form action="index.html" method="get">
						<button class="btn btn-default btn-green" type="submit">Contratar</button>
					</form>
				</div>
			</div>
			<div class="col-md-3 costs">
				<div data-stellar-horizontal-offset="0" data-stellar-vertical-offset="150" data-stellar-ratio="1.2" style="top: 0px;">
					<img alt="Extra large dog pricing" src="/img/template/xlarge-dog.png">
					<h4>Plano Max</h4>
					<p class="per-night">R$ 80 ao mês</p>
					<p>Agendamento Online</p>
					<p>Prontontuário Eletônico</p>
					<p>Agenda de consultas</p>
					<p>6 acessos simultâneos</p>
					<p>Envio de emails de consulta</p>
					<p>200 SMS's para envio</p>
					<p>Envio de SMS de confirmação</p>
					<p>Resposta de SMS gratuita</p>
					<form action="index.html" method="get">
						<button class="btn btn-default btn-green" type="submit">Contratar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span>Contratações Adicionais</span></h3>
			<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
		</div>
	</div>
</div>

<div class="br-2"></div>

<div class="container">
	<div class="row services-single margin-top-zero">
		<div class="col-md-6">
			<h2 class="margin-bottom-zero">Acessos adicionais</h2>
			<div class="additional">
				<div>
					<p class="left"><strong>+1 acesso simultâneo ao sistema</strong></p>
					<p class="right glyphicon glyphicon-user"> R$ 5,00 ao mês</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="br-1"></div>

<div class="container">
	<div class="row services-single margin-top-zero">
		<div class="col-md-6">
			<h2 class="margin-bottom-zero">SMS's adicionais</h2>
			<div class="additional">
				<div>
					<p class="left"><strong>+50 SMS's</strong></p>
					<p class="right glyphicon glyphicon-send"> R$ 25,00 ao mês</p>
				</div>
				<div>
					<p class="left"><strong>+100 SMS's</strong></p>
					<p class="right glyphicon glyphicon-send"> R$ 40,00 ao mês</p>
				</div>
				<div>
					<p class="left"><strong>+200 SMS's</strong></p>
					<p class="right glyphicon glyphicon-send"> R$ 70,00 ao mês</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="active-link" active="li-professionals"></div>