<!-- Contact -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span>Fale conosco</span></h3>
			<p>Aqui você pode deixar seus comentários, reclamações e sugestões sobre o site. Estamos ansiósos para conversarmos com você. Veja como é fácil entrar em contato: Nos campos abaixo, digite seu nome, email, telefone e sua mensagem, depois é só clicar em enviar mensagem e pronto.</p>
		</div>
	</div>
</div>
<!-- Contact end -->

<div class="br-1"></div>

<!-- Content -->
<div class="container content">
	<div class="row">
		<div class="col-md-9">
			<form id="contact_form" action="/contacts/add" method="post" role="form">
				<div class="form-group">
					<label for="InputName">Seu Nome</label>
					<?php echo $this->form->input('Contact.name', array('class' => 'form-control', 'placeholder' => 'Seu Nome', 'div' => false, 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label for="InputEmail">Seu Email</label>
					<?php echo $this->form->email('Contact.email', array('class' => 'form-control', 'placeholder' => 'Seu Email', 'div' => false, 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label for="InputPhone">Seu Telefone</label>
					<?php echo $this->form->text('Contact.phone', array('class' => 'form-control', 'placeholder' => 'Seu Telefone', 'div' => false, 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label for="InputPhone">Assunto</label>
					<?php echo $this->Form->input('Contact.subject', array('options' => array('Dúvidas' => 'Dúvidas', 'Reclamações' => 'Reclamações', 'Sugestões' => 'Sugestões', 'Outros' => 'Outros'), 'empty' => 'Selecione o assunto', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label for="InputMesaagel">Sua Mensagem</label>
					<?php echo $this->form->textarea('Contact.message', array('class' => 'form-control', 'placeholder' => 'Sua Mensagem', 'rows' => '8', 'div' => false, 'label' => false)); ?>
				</div>
				<button type="submit" class="btn btn-default btn-green">Enviar Mensagem</button>
			</form>
		</div>
		<div class="col-md-3">
			<ul class="contact-info">
				<li class="telephone">
					(11) 9 8326-3904
				</li>
				<!-- <li class="address">
					Rua Alecrim, 49 - São Paulo - SP
				</li> -->
				<li class="mail">
					contato@veterinariapet.com.br
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- Content end -->
<div class="br-5"></div>

<!-- Mostra o link ativo no menu -->
<div id="active-link" active="li-contact"></div>