<?php

class Contact extends AppModel {
	public $name = 'Contact';
	public $useTable = 'contact_requests';

	public $actsAs = array('Containable');
}