<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php echo $this->Html->charset(); ?>
	<title>
		VeterináriaPet
	</title>

	<?php 
		echo $this->Html->css(
			array(
				'bootstrap.min.css', 
				'style.css',
				'http://fonts.googleapis.com/css?family=Cabin:400,500,600,700,400italic,500italic,600italic,700italic', 
				'http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic,700italic,900italic', 
				'main.css',
			)
		);
	?>

</head>
<body class="homepage">
	<?php echo $this->Session->flash(); ?>

	<?php echo $this->element('header'); ?>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element('footer'); ?>

	<?php 
		echo $this->Html->script(
			array(
				'jquery-1.11.1.min.js',
				'bootstrap.min.js',
				'carouFredSel.js',
				'jquery.stellar.min.js',
				'ekkoLightbox.js',
				'custom.js',
				'main.js',
			)
		);
	?>
</body>