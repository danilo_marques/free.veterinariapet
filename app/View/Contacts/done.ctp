<div class="rehome margin-top-zero">
	<div class="container">
		<div class="row">
			<div class="col-md-12 centered col3">
				<a id="success" class="roundal" title="Mensagem Enviada" href="javascript:void(0);"></a>
				<h4>Sua mensagem foi enviada com sucesso!</h4>
				<p>Sua mensagem foi enviada com sucesso e será analizada por um de nossos funcinários e caso necessário entrará em contato para poder melhor atende-lo. Desde já a <strong>VeterináriaPet</strong> lhe deseja muito obrigado.</p>

				<a class="btn btn-default btn-green" href="/contato/">Enviar Nova Mensagem</a>
				<div class="br-1"></div>
			</div>
		</div>
	</div>
	<div class="br-4"></div>
</div>

<!-- Mostra o link ativo no menu -->
<div id="active-link" active="li-contact"></div>