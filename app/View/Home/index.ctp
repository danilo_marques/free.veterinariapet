<!-- Slider -->
<div id="home_carousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#home_carousel" data-slide-to="0" class="active"></li>
		<li data-target="#home_carousel" data-slide-to="1"></li>
		<li data-target="#home_carousel" data-slide-to="2"></li>
	</ol>
	
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<img src="/img/template/1.jpg" alt="" />
			<div class="carousel-caption">
				<h2>Porque Pet é Tudo de Bom ^.^</h2>
			    <p>O VeterináriaPet é um produto voltado para a saúde e bem estar de seu bichinho de estimação, onde você encontra as melhores clínicas veterinárias para o seu Pet. Conheça melhor o Veterinária Pet e começa agora mesmo a aproveitar os benefícios do site.</p>
			    <form method="get" action="/sobre">
			    	<button type="submit" class="btn btn-lg btn-default">Saiba Mais</button>
			    	</form>
			</div>
		</div>
		<div class="item">
			<img src="/img/template/2.jpg" alt="" />
			<div class="carousel-caption">
				<h2>Pet é tudo de bom ^.^</h2>
			    <p>O VeterináriaPet é um produto voltado para a saúde e bem estar de seu bichinho de estimação, onde você encontra as melhores clínicas veterinárias para o seu Pet. Conheça melhor o Veterinária Pet e começa agora mesmo a aproveitar os benefícios do site.</p>
			    <form method="get" action="/sobre">
			    	<button type="submit" class="btn btn-lg btn-default">Saiba Mais</button>
			    </form>
			</div>
		</div>
		<div class="item">
			<img src="/img/template/3.jpg" alt="" />
			<div class="carousel-caption">
				<h2>Pet é tudo de bom ^.^</h2>
			    <p>O VeterináriaPet é um produto voltado para a saúde e bem estar de seu bichinho de estimação, onde você encontra as melhores clínicas veterinárias para o seu Pet. Conheça melhor o Veterinária Pet e começa agora mesmo a aproveitar os benefícios do site.</p>
			    <form method="get" action="/sobre">
			    	<button type="submit" class="btn btn-lg btn-default">Saiba Mais</button>
			    </form>
			</div>
		</div>
	</div>
	
	<!-- Controls -->
	<a class="left carousel-control" href="#home_carousel" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#home_carousel" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<!-- Slider end -->

<?php echo $this->element('search'); ?>

<!-- About -->
<div class="rehome margin-top-zero">
	<div class="container">
		<div class="row">
			<div class="row services-single">
				
				<div class="col-md-6">
					<img alt="Dog walking" src="/img/template/dog-10.png">
				</div>
				<div class="col-md-6">
					<h4>O que é o <strong>Veterinária</strong>Pet?</h4>
					<p style="margin-bottom: 12px;">O VeterináriaPet é um sistema de prontuário veterinário e sistema de agendamento online voltado para a área de medicima animal que tem por objetivo facilidar o dia a dia do profissionais do ramo e também dos donos de animais de estimação.</p>

					<p style="margin-bottom: 12px;"><strong>Para Profissionais:</strong></p>
					<p style="margin-bottom: 12px;">O VeterináriaPet oferece um sistema de prontuário online como com um agenda de marcações de consultas que pode ser utilizada tanto para marcar seus agendamentos quanto para receber os agendamentos vindos pelo site.</p>

					<p style="margin-bottom: 12px;"><strong>Para Donos de Animais de Estimação:</strong></p>
					<p style="margin-bottom: 12px;">O VeterináriaPet oferece um sistema de prontuário online como com um agenda de marcações de consultas que pode ser utilizada tanto para marcar seus agendamentos quanto para receber os agendamentos vindos pelo site.</p>
				</div>
				<div class="br-4">&nbsp;</div>
			</div>
		</div>
	</div>
</div>
<!-- About - end -->

<!-- Services -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span>Vantagens do <strong>Veterinária</strong>Pet</span></h3>
		</div>
		<div class="col-md-4 col3" style="margin-top: 50px;">
			<a href="services-single.html" title="Facilidade e Comodidade" class="roundal" id="kennel"></a>
			<h3>Facilidade e Comodidade</h3>
			<p>Encontre as melhores clínicas veterinárias mais próximas de você sem precisar sair do conforto de sua casa.</p>
		</div>
		<div class="col-md-4 col3" style="margin-top: 50px;">
			<a href="services-single.html" title="Agendamento Online" class="roundal" id="adoption"></a>
			<h3>Agendamento Online</h3>
			<p>Marque um consulta para o seu animalzinho de maneira fácil e rápida, tudo isso em poucos minutos.</p>
		</div>
		<div class="col-md-4 col3" style="margin-top: 50px;">
			<a href="services-single.html" title="Serviços Especiais" class="roundal" id="walking"></a>
			<h3>Serviços Especiais</h3>
			<p>Desfrute de alguns serviços oferecidos juntamente com o agendamento, como o serviço leva e traz.</p>
		</div>
	</div>
</div>
<!-- Services end -->

<!-- Carousel -->
<div class="container">
	<div class="row">
		<div class="col-md-12 centered">
			<h3><span>Pets preferidos pelas pessoas</span></h3>
			<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
		</div>
	</div>
</div>
<div id="c-carousel">
	<div id="wrapper">
		<div id="carousel">
			<div>
				<a href="/img/template/dog-1.png" title="Dog" data-hover="Sandy the west highland terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-1.png" alt="Dog" />
				</a>
			</div>
			<div>
				<a href="/img/template/dog-2.png" title="Dog" data-hover="Marty the yorkshire terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-2.png" alt="Dog" />
				</a>
			</div>
			<div>
				<a href="/img/template/dog-3.png" title="Dog" data-hover="Kyla the bull terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-3.png" alt="Dog" />
				</a>
			</div>
			<div>
				<a href="/img/template/dog-1.png" title="Dog" data-hover="Marty the yorkshire terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-1.png" alt="Dog" />
				</a>
			</div>
			<div>
				<a href="/img/template/dog-2.png" title="Dog" data-hover="Sandy the west highland terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-2.png" alt="Dog" />
				</a>
			</div>
			<div>
				<a href="/img/template/dog-3.png" title="Dog" data-hover="Kyla the bull terrier" data-toggle="lightbox" class="lightbox">
					<img src="/img/template/dog-3.png" alt="Dog" />
				</a>
			</div>
		</div>
		<div id="pager" class="pager"></div>
	</div>
</div>
<!-- Carousel end -->
<div class="br-2"></div>

<!-- Mostra o link ativo no menu -->
<div id="active-link" active="li-index"></div>