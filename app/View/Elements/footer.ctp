<!-- Purchase -->
<div class="purchase margin-top-zero" style="padding: 30px 0;">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<p>Contrate o sistema <strong>Veterinária</strong>Pet e ganhe o agendamento online gratis<br />
				<span>Para maiores informações sobre como contratar os serviços, clique no botão mais informações.</span></p>
			</div>
			<div class="col-md-3 purchase-button">
				<form method="get" action="/profissionais">
					<button type="submit" class="btn btn-default btn-green">Mais Informações</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Purchase end -->

<!-- Footer -->
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h6>Um Pouco Sobre Nós</h6>
				<p><strong>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat.</strong></p>
				<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam.</p>
			</div>
			<div class="col-md-3 blog">
				<h6>Nosso Blog</h6>
				<p class="title"><a href="#" title="">Eodem modo typi, qui nunc nobis</a></p>
				<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram.</p>
				<p><a href="#" title="">Read this post&hellip;</a></p>
			</div>
			<div class="col-md-3">
				<h6>Você Pode Precisar</h6>
				<ul>
					<li><a href="#" title="">Início</a></li>
					<li><a href="#" title="">Buscar Clínica</a></li>
					<li><a href="#" title="">Para Profissionais</a></li>
					<li><a href="#" title="">Sobre a Empresa</a></li>
					<li><a href="#" title="">Contato</a></li>
				</ul>
			</div>
			<div class="col-md-3 contact-info">
				<h6>Mantenha Contato</h6>
				<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
				<p class="social">
					<a href="#" class="facebook"></a> <a href="#" class="pinterest"></a> <a href="#" class="twitter"></a>
				</p>
				<p class="c-details">
					<span>E-Mail</span> &nbsp;<a href="#" title="email">contato@veterinariapet.com.br</a><br >
					<span>Fone</span> (11) 9 8326-3904
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 copyright">
				<p>&copy; Veterinária Pet 2014 - Todos os direitos reservados.</p>
			</div>
		</div>
	</div>
</div>
<!-- Footer end -->