<?php

class ContactsController extends AppController {

	public $uses = array('Contact');

	/* Página de contato */
	public function index() {}

	/* Salva um contato */
	public function add() {
		if($this->data) {
			if($this->Contact->save($this->data)) {
				$this->redirect('/contato/sucesso/');
			}
		}
	}

	/* Mensagem de formulário enviado com sucesso */
	public function done() {}

	/* Página "Para Profissionais" */
	public function professional() {}
}