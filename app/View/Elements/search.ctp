<!-- Search -->
<div class="container">
	<div class="row margin-to-zero" style="margin-top: -30px;">
		<div class="col-md-12 centered">
			<h3><span>Encontre um veterinário perto de você</span></h3>
			<p>Aqui você pode encontrar uma clínica veterinária próxima de sua residência de maneira rápida, fácil e eficiênte. Além disso também é possível agendar uma consulta para o seu animalzinho diretamente pelo site e também escolher se deseja utilizar o sistema leva e traz caso a clínica disponibilize e tudo isso com poucos cliques.</p>
		</div>
	</div>
</div>
<div data-stellar-background-ratio="0.6" class="testimonials" style="background-position: 0px 97.2601px; margin-top: 30px !important;">
	<div class="br-1"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- ARRUMAR -->
				<br /><br /><br />
				<!-- ARRUMAR FIM -->
				<form id="vp-form-search" role="form" action="/busca" method="get">
					<div class="col-md-7">
						<div class="form-group">
							<label for="InputName">Clínica próxima ao endereço:</label>
							<?php echo $this->form->input('Address.address', array('class' => 'form-control', 'placeholder' => 'Endereço', 'div' => false, 'label' => false)); ?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="InputName">Especialidade:</label>
							<!-- <input type="text" placeholder="Especialidade" id="InputName" class="form-control"> -->
							<?php echo $this->form->input('Specialty.name', array('options' => array('1' => 'Cardiologia'), 'class' => 'form-control', 'empty' => 'Selecionar', 'div' => false, 'label' => false)); ?>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label for="button">&nbsp;</label>
							<button class="btn btn-default btn-green btn-search" type="submit">Buscar</button>
						</div>
					</div>
				</form>
				<!-- ARRUMAR -->
				&nbsp;<br />
				&nbsp;<br />
				&nbsp;<br />
				<!-- ARRUMAR FIM -->
			</div>
		</div>
	</div>
	<div class="br-1"></div>
</div>
<!-- Search end -->