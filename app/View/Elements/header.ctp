<!-- Navigation -->
<div class="navbar navbar-default navbar-fixed-top affix inner-pages" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<h1>
				<a class="navbar-brand" href="/"><strong>Veterinária</strong>Pet
					<br />
					<span>&nbsp;Porque Pet é Tudo de Bom ^.^</span>
				</a>
			</h1>
		</div>	
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="li-index">
					<a href="/" title="Início"><span data-hover="Início">Início</span></a>
				</li>
				<li class="li-search">
					<a href="/busca" title="Clínicas"><span data-hover="Buscar Clínica">Buscar Clínica</span></a>
				</li>
				<li class="li-professionals">
					<a href="/profissionais" title="Profissionais"><span data-hover="Para Profissionais">Para Profissionais</span></a>
				</li>
				<li class="li-about">
					<a href="/sobre" title="Sobre"><span data-hover="Sobre a Empresa">Sobre a Empresa</span></a>
				</li>
				<li class="li-contact">
					<a href="/contato/" title="Contato"><span data-hover="Contato">Contato</span></a>
				</li>
				<li class="purchase-btn">
					<form method="get" action="/login">
						<button type="submit" class="btn btn-default">Fazer Login</button>
					</form>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- Navigation end -->