<?php

	/**
	 * Routes configuration
	 */
	Router::connect('/', array('controller' => 'Home', 'action' => 'index'));

	/* Contato */
	Router::connect('/contato', array('controller' => 'Contacts', 'action' => 'index'));
	Router::connect('/contato/sucesso', array('controller' => 'Contacts', 'action' => 'done'));

	/* Busca */
	Router::connect('/busca', array('controller' => 'Search', 'action' => 'index'));

	/* Para Profissionais */
	Router::connect('/profissionais', array('controller' => 'Contacts', 'action' => 'professional'));

	/* Sobre a empresa */
	Router::connect('/sobre', array('controller' => 'About', 'action' => 'index'));

	/* Login */
	Router::connect('/login', array('controller' => 'Users', 'action' => 'index'));

	/**
	 * ...and connect the rest of 'Pages' controller's URLs.
	 */
	// Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';
